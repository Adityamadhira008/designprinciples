//implement DRY principle
import java.util.Random;
import java.util.Scanner;

class randomgen{
    Random rand=new Random();
    int gen(){
        return rand.nextInt(100);
    }
}

public class DRY {
    public static void main(String args[]){
        Scanner mys=new Scanner(System.in);
        System.out.println("enter loop num");
        randomgen obj=new randomgen();
        int i=mys.nextInt();
        for(int x=0;x<i;x++){
            int ans=obj.gen();
            System.out.println(ans);

        }

    }

}
