//Take and capitalize name using KISS principle
import java.util.*;

public class KISS {
    public static void main(String args[]){
        String name,cname;
        Scanner mys=new Scanner(System.in);
        System.out.println("Enter name");
        name=mys.nextLine();
        cname=name.toUpperCase();
        System.out.println(cname);
    }
}
