//java program implementing SOLID principles
//Single Responsibility(Each class is having single responsibility)
class car{
    void func(){
        System.out.println("car");
    }
    //open-closed,extedending class without modifying its behaviour
    void priceret(){
        System.out.println("$40000");
    }

}
//Interface segmentation
class carpick{
    void disp(){
        System.out.println("1-Tesla,2-Bmw,3-Buggati");
    }
}
class bike{
    void func(){
        System.out.println("bike");
    }
    void priceret(){
        System.out.println("$20000");
    }
}

class carchild extends car{
    void test(){
        System.out.println("child class of car");
    }
}
public class SOLID {
    public static void main(String args[]){
        //Liskov Substitution(Child should perform everything a parent can)
        car obj=new car();
        carchild obj_2=new carchild();
        obj.func();
        obj_2.func();

    }
}
