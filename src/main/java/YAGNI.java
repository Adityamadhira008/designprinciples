import java.util.*;

class display{
    void Dis(){
        System.out.println("Time Up");
    }
}
public class YAGNI {
    public static void main(String args[]){
        Scanner mys=new Scanner(System.in);
        System.out.println("Enter time");
        int c=mys.nextInt();
        System.out.println("Sleeping for"+c+"seconds.......");

        for (int i=1;i<=c;i++){
            System.out.println(i+"seconds has elapsed");
        }
        //YAGNI(creating object only when necessary)
        display obj=new display();
        obj.Dis();


    }
}
